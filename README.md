# Backbase Front End Assignment: Make A Transaction


## Brief Description

This project was created using Angularjs 1.6, specifically using a component based design. Some of the concepts displayed in this demo includes modular design concepts,
component routing, and data binding.

## Getting Started
All code and css has been compiled and minimized.  Project can easily be view with a simple local server or by running `npm install` and then `npm start` which will launch BrowserSync. 

Run on localhost: http://localhost:<port>/public/#!/

Run with npm start: http://localhost:8000/#!/

### Transfering Money
User can select a merchant from the dropdown and transfer money.  Implemented some basic validation so that only valid amounts can be transferred and to make sure
transfer amount does not exceed balance.   Unfortunately, ran out of time to implement error message.

### Transaction History
History reflects latest transactional updates.  Colors appeared random on provided PSD, so a random color is generated for each row. 


