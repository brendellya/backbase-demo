var  gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  minifycss = require('gulp-minify-css'),
  less = require('gulp-less'),
  rename = require('gulp-rename'),
  babel = require('gulp-babel'),
  sourcemaps = require('gulp-sourcemaps'),
  ngAnnotate = require('gulp-ng-annotate'),
  browserSync = require('browser-sync').create();

var sourceDir = 'public';
var distDir = 'public/assets/dist';

var watchFiles = ['public/index.html', 'public/index.js', 'public/index.less', 'public/app/**/*.html', 'public/app/**/*.js', 'public/app/**/*.less', 'public/assets/scr/**/*.less'];

var vendorCSS = [
  'node_modules/font-awesome/css/font-awesome.css',
  'node_modules/bootstrap/dist/css/bootstrap.css'
];

var vendorJS = [
  'node_modules/angular/angular.js',
  'public/assets/src/js/ui-router.js'
];

var port = 8000;

gulp.task('compile:less', function(){
  return gulp.src(sourceDir + '/app/app.less')
    .pipe(less())
    .pipe(minifycss())
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest(distDir + '/css/'));
});

gulp.task('compile:css', function(){
  return gulp.src(vendorCSS)
  .pipe(concat('vendor.css'))
  .pipe(minifycss())
  .pipe(gulp.dest(distDir + '/css/'));
});

gulp.task('compile:vendor', function(){
  return gulp.src(vendorJS)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(rename('vendor.min.js'))
    .pipe(gulp.dest(distDir + '/js/'));
});

gulp.task('compile:app', function(){
  return gulp.src([
    sourceDir + '/index.js',
    sourceDir + '/app/**/*.module.js',
    sourceDir + '/app/**/*.comp.js',
    sourceDir + '/app/**/*.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(rename('app.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(distDir + '/js/'));
});

gulp.task('build:dev', ['compile:less', 'compile:css', 'compile:vendor', 'compile:app']);

gulp.task('watch', ['build:dev'], function(done){
  browserSync.reload();
  done();
});

gulp.task('server:client', ['build:dev'], function () {
  browserSync.init({
    port: port,
    server: {
      baseDir: sourceDir
    }
  });

  gulp.watch(watchFiles, ['watch']);
});


// Default Tasks
gulp.task('default', ['server:client']);
