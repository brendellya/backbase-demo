'use strict';

/**
 * Created by Brendellya on 4/1/2017.
 */

angular.module('app', ['common', 'core']);

/**
 * Created by Brendellya on 4/1/2017.
 */

angular.module('common', ['ui.router']);

/**
 * Created by Brendellya on 4/1/2017.
 */

angular.module('core', ['ui.router', 'core.transfer']);

/**
 * Created by Brendellya on 4/2/2017.
 */

// Todo Change module
angular.module('core.transfer', []);

/**
 * Created by Brendellya on 4/1/2017.
 */

var appRoot = {
  template: '<div class="root container-fluid"><app-header></app-header><div ui-view></div></div>'
};

angular.module('app').component('appRoot', appRoot);

/**
 * Created by Brendellya on 4/1/2017.
 */

var appHeader = {
  templateUrl: 'app/common/header/app-header.html'
};

angular.module('common').component('appHeader', appHeader);

/**
 * Created by Brendellya on 4/1/2017.
 */

var appHome = {
  bindings: {
    transfers: '<',
    balance: '<'
  },
  templateUrl: 'app/common/home/app-home.html'
};

angular.module('common').component('appHome', appHome).config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider.state('home', {
    url: '/',
    component: 'appHome',
    resolve: {
      transfers: function transfers(TransferService) {
        return TransferService.getData().then(function () {
          return TransferService.cache.transfers;
        });
      },
      account: function account(TransferService) {
        return TransferService.account;
      }
    }
  });

  $urlRouterProvider.otherwise('/');
});

/**
 * Created by Brendellya on 4/1/2017.
 */

var transfer = {
  bindings: {
    transfers: '<'
  },
  transclude: true,
  templateUrl: 'app/core/transfer/transfer.html',
  controller: 'transferCtrl'
};

angular.module('core').component('transfer', transfer);

/**
 * @ngdoc directive
 * @name app.home.directive:yuiLivesearch
 * @description General live search dropdown
 * @restrict 'AE'
 * @element ANY
 * @scope
 *
 * @description Live search dropdown
 *
 * @requires $analytics
 * @requires utilityFactory
 *
 */
// angular.module('common').directive('yuiLivesearch', function($analytics, utilityFactory){
//     return {
//         restrict: 'AE',
//         replace: true,
//         scope: {
//             formName: '@',
//             options: '=',  // array of options
//             selection: '=', // default selection (object)
//             name: '@', // property to display in dropdown list
//             required: '@', // can't deselected a set value
//             disabled: '=', // disabled
//             error: '=', // error
//             inputDisabled: '@', //
//             callback: '&', // invoke callback on item select
//             placeholder: '@',
//             yuianalyticscategory: '@',
//             yuianalyticslabel: '@'
//         },
//         templateUrl: "/app/component/yui-livesearch/yui-livesearch.html",
//         link: function(scope, element, attrs, fn) {
//             var listContainer = element[0].querySelector('ul');
//             var itemHeight = 34;
//             var rawOptions = [];
//
//             var keys = {
//                 40: 'DOWN',
//                 38: 'UP',
//                 13: 'ENTER'
//             };
//
//             var emptyRow = {
//                 Value: null
//             };
//
//             scope.activeIndex = 0;
//             scope.listVisible = false;
//             scope.name = scope.name || "Name";
//             scope.listOptions = [];
//
//
//             /**
//              * @ngdoc method
//              * @name filterOptions
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Filter options array based on search input.
//              *
//              * @param {array} options options
//              * @param {string} str searchname
//              * @returns {array} options filtered
//              */
//             function filterOptions(options, str) {
//                 var result;
//
//                 // Set empty/default row if enabled and not already found
//                 if (!scope.required) {
//                     var found  = utilityFactory.doArrayFilterBy(options, scope.name, emptyRow[scope.name]);
//                     if (!found.length){
//                         options.unshift(emptyRow);
//                     }
//                 }
//
//                 //  Return original options if not search string
//                 if(!str || str === emptyRow[scope.name]) return options;
//
//                 // Return filter search string
//                 result = options.reduce(function(memo, opt){
//                     if(opt.Name.indexOf(str) > -1 || opt.Name === emptyRow[scope.name]){
//                         memo.push(opt);
//                     }
//                     return memo;
//                 }, []);
//
//                 return (result.length) ? result : options;
//             }
//
//             /**
//              * @ngdoc method
//              * @name scrollList
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Adds key events to dropdown.  Scrolls
//              * list up/down. Enter toggles list open & selects item.
//              *
//              * @param {string} dir directions
//              */
//             function scrollList(dir){
//                 var items = listContainer.querySelectorAll('li');
//                 var itemsArr = Array.prototype.slice.call(items);
//                 var offset = listContainer.clientHeight - itemHeight;
//
//                 if (dir === 'DOWN'){
//                     scope.activeIndex = (scope.activeIndex < itemsArr.length -1) ? scope.activeIndex + 1 : itemsArr.length - 1;
//                 } else if (dir === 'UP'){
//                     scope.activeIndex = (scope.activeIndex < 1) ? 0 : scope.activeIndex - 1;
//                 }
//
//                 if (dir === 'ENTER'){
//                     if(!scope.listVisible){
//                         scope.showSelection(scope.listVisible);
//                     } else {
//                         scope.selectItem(scope.listOptions[scope.activeIndex], scope.activeIndex);
//                     }
//                 }
//
//                 // Offset value keeps scroll hilite within view
//                 listContainer.scrollTop = (scope.activeIndex * itemHeight < offset) ? 0 : (scope.activeIndex * itemHeight) - offset;
//             }
//
//             /**
//              * @ngdoc method
//              * @name initLivesearch
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Init livesearch.  Copies scope.options
//              * preserving original values.  Sets default empty
//              * display name.
//              *
//              */
//             function initLivesearch(){
//                 scope.inputDisabled = (scope.inputDisabled === 'true') ? true : false;
//                 rawOptions = angular.copy(scope.options);
//
//                 // Set Empty Row if value can be null
//                 emptyRow[scope.name] = scope.placeholder || '...';
//                 scope.listOptions = filterOptions(rawOptions, null);
//             }
//
//             /**
//              * @ngdoc method
//              * @name keyAction
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Captures keydown events when
//              * the dropdown list is visible.
//              * Invokes scrollList.
//              *
//              * @param {object} evt event object
//              */
//             scope.keyAction = function(evt){
//                 var evtCode = evt.keyCode;
//
//                 if(keys[evtCode]){
//                     scrollList(keys[evtCode]);
//                     evt.preventDefault();
//                 }
//             };
//
//             /**
//              * @ngdoc method
//              * @name searchOptions
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Captures change events on input
//              * form.  Invokes filterOptions, and resets the
//              * dropdown list based on input.
//              *
//              */
//             scope.searchOptions = function() {
//                 scope.listOptions = filterOptions(rawOptions, scope.selectionName);
//             };
//
//             /**
//              * @ngdoc method
//              * @name showSelection
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Toggles the list view & backdrop
//              * on/off.
//              *
//              * @param {boolean} show toggles listview
//              */
//             scope.showSelection = function(show) {
//                 if(scope.disabled) return;
//                 scope.listVisible = !show;
//
//                 // If dropdown is close before making a selection, select a valid option
//                if(scope.selectionName !== scope.selection[scope.name]){
//                 scope.selectItem(scope.listOptions[scope.activeIndex], scope.activeIndex);
//                }
//             };
//
//             /**
//              * @ngdoc method
//              * @name selectItem
//              * @methodOf app.home.directive:yuiLivesearch
//              * @description Changes selected item, and invokes
//              * scope.callback if set. Toggles list to off.
//              * And sets the activeIndex for list items.
//              *
//              * @param {object} item list item
//              * @param {number} index list item index
//              *
//              */
//             scope.selectItem = function(item, index) {
//                 var oldSelection = scope.selection;
//                 oldSelection.Selected = false;
//
//                 item.Selected = true;
//                 scope.selection = item;
//                 scope.listVisible = false;
//                 scope.activeIndex = index || 0;
//
//                 // Reflect updates to input name
//                 scope.selectionName = item[scope.name];
//
//                 if(scope.callback){
//                     scope.callback({ item: scope.selection });
//                 }
//
//                 // if(scope.yuianalyticscategory && scope.yuianalyticslabel){
//                 //     $analytics.eventTrack('change', { category: scope.yuianalyticscategory, label: scope.yuianalyticslabel, value: 1});
//                 // }
//
//             };
//
//
//             initLivesearch();
//
//             /**
//              * @ngdoc event
//              * @name selection
//              * @eventOf app.home.directive:yuiLivesearch
//              * @description Watches selection
//              */
//             scope.$watch('selection', function(newVal, oldVal){
//                 if(newVal){
//                     if(scope.name){
//                         scope.selectionName = scope.selection[scope.name];
//                     }else{
//                         scope.selectionName = scope.selection.Name;
//                     }
//                 }
//             });
//
//             /**
//              * @ngdoc event
//              * @name options
//              * @eventOf app.home.directive:yuiLivesearch
//              * @description Watches options
//              */
//             scope.$watch('options', function(newVal, oldVal){
//                 if(newVal){
//                     initLivesearch();
//                 }
//             });
//         }
//     };
// });

/**
 * Created by Brendellya on 4/2/2017.
 */

var transferForm = {
  bindings: {
    account: '='
  },
  require: {
    parent: '^transfer'
  },
  templateUrl: 'app/core/transfer/transfer-form/transfer-form.html',
  controller: 'transferFormCtrl'
};

angular.module('core').component('transferForm', transferForm);

/**
 * Created by Brendellya on 4/2/2017.
 */

var transferHistory = {
  require: {
    parent: '^transfer'
  },
  templateUrl: 'app/core/transfer/transfer-history/transfer-history.html',
  controller: 'transferHistoryCtrl'
};

angular.module('core').component('transferHistory', transferHistory);

/**
 * Created by Brendellya on 4/1/2017.
 */
function transferCtrl(TransferService) {
  var ctrl = this;

  ctrl.account = TransferService.account;
  ctrl.merchants = [];

  // Transaction Form
  ctrl.selectedMerchant = {};

  function resetForm() {
    ctrl.isEditing = true;
    ctrl.amount = "";
  }

  ctrl.confirmTransfer = function () {
    var payload = {
      amount: ctrl.amount,
      merchant: ctrl.selectedMerchant.merchant,
      merchantLogo: ctrl.selectedMerchant.merchantLogo,
      transactionDate: new Date().getTime(),
      transactionType: 'Online Transfer',
      color: 'hsl(' + Math.ceil(Math.random() * 360) + ',50%,50%)'
    };

    TransferService.postTransfer(payload);
    resetForm();
  };

  ctrl.submitTransfer = function () {
    ctrl.isEditing = false;
  };

  ctrl.$onInit = function () {
    resetForm();
  };

  ctrl.$onChanges = function (changes) {
    if (changes.transfers) {

      ctrl.merchants = TransferService.getMerchants();
      ctrl.selectedMerchant = ctrl.merchants[0];
    }
  };
}

angular.module('core').controller('transferCtrl', transferCtrl);

/**
 * Created by Brendellya on 4/1/2017.
 */
function TransferService($http) {
  var url = 'data/transactions.json';

  function getUniqueItems(list, keyword) {
    var found = [];
    if (!list.length) return [];

    return list.reduce(function (memo, row) {
      if (found.indexOf(row.merchant) < 0) {
        memo.push({
          merchant: row.merchant,
          merchantLogo: row.merchantLogo
        });

        found.push(row.merchant);
      }
      return memo;
    }, []);
  }

  function preProccessData(data) {
    return data.map(function (row) {
      var hue = Math.ceil(Math.random() * 360);
      var hsl = 'hsl(' + hue + ',50%,50%)';

      row.color = hsl;
      return row;
    });
  }

  return {
    // Local Cache
    cache: {
      transfers: []
    },

    // Fake Account
    account: {
      name: 'Free Checking(4692)',
      balance: 5824.76
    },

    updateBalance: function updateBalance(payload) {
      var newBalance = this.account.balance - payload.amount;
      this.account.balance = newBalance.toFixed(2);
    },

    getData: function getData() {
      var _this = this;

      return $http.get(url).then(function (res) {
        var data = res.data.data || [];

        // Preprocess Transfer Data
        _this.cache.transfers = preProccessData(data);
      });
    },

    postTransfer: function postTransfer(payload) {
      this.cache.transfers.unshift(payload);
      this.updateBalance(payload);
    },

    getMerchants: function getMerchants() {
      return getUniqueItems(this.cache.transfers, 'merchant');
    }
  };
}

angular.module('core').factory('TransferService', TransferService);

/**
 * Created by Brendellya on 4/2/2017.
 */
function transferFormCtrl() {
  var ctrl = this;

  ctrl.validateAmount = function ($input) {
    var amount = $input.$modelValue;
    var balance = Number(ctrl.account.balance);

    if (!isNaN(amount) && amount > 0 && amount <= balance) {
      $input.$setValidity('required', true);
    } else {
      $input.$setValidity('required', false);
    }
  };
}

angular.module('core').controller('transferFormCtrl', transferFormCtrl);

/**
 * Created by Brendellya on 4/2/2017.
 */

function transferHistoryCtrl() {
  var ctrl = this;
}

angular.module('core').controller('transferHistoryCtrl', transferHistoryCtrl);