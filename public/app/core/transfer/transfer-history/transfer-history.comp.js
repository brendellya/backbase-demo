/**
 * Created by Brendellya on 4/2/2017.
 */

var transferHistory = {
  require: {
    parent: '^transfer'
  },
  templateUrl: 'app/core/transfer/transfer-history/transfer-history.html',
  controller: 'transferHistoryCtrl'
};

angular.module('core.transfer').component('transferHistory', transferHistory);
