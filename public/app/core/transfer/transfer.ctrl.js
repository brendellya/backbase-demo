/**
 * Created by Brendellya on 4/1/2017.
 */
function transferCtrl(TransferService) {
  var ctrl = this;

  ctrl.account = TransferService.account;
  ctrl.merchants = [];

  // Transaction Form
  ctrl.selectedMerchant = {};


  function resetForm(){
    ctrl.isEditing = true;
    ctrl.amount = "";
  }


  ctrl.confirmTransfer = function(){
    var payload = {
      amount: ctrl.amount,
      merchant: ctrl.selectedMerchant.merchant,
      merchantLogo: ctrl.selectedMerchant.merchantLogo,
      transactionDate: new Date().getTime(),
      transactionType: 'Online Transfer',
      color: 'hsl(' +Math.ceil(Math.random() * 360)+ ',50%,50%)'
    };

      TransferService.postTransfer(payload);
      resetForm();
  };

  ctrl.submitTransfer = function(){
    ctrl.isEditing = false;
  };

  ctrl.$onInit = function () {
    resetForm();
  };

  ctrl.$onChanges = function(changes){
    if(changes.transfers){

      ctrl.merchants = TransferService.getMerchants();
      ctrl.selectedMerchant = ctrl.merchants[0];
    }
  };

}


angular.module('core.transfer').controller('transferCtrl', transferCtrl);
