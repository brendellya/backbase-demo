/**
 * Created by Brendellya on 4/2/2017.
 */

var transferForm = {
  bindings: {
    account: '='
  },
  require: {
    parent: '^transfer'
  },
  templateUrl: 'app/core/transfer/transfer-form/transfer-form.html',
  controller: 'transferFormCtrl'
};

angular.module('core.transfer').component('transferForm', transferForm);
