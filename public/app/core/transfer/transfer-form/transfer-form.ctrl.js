/**
 * Created by Brendellya on 4/2/2017.
 */
function transferFormCtrl () {
  var ctrl = this;


  ctrl.validateAmount = function($input){
    var amount = $input.$modelValue;
    var balance = Number(ctrl.account.balance);

    if(!isNaN(amount) && (amount > 0 && amount <= balance)){
      $input.$setValidity('required', true);
    } else {
      $input.$setValidity('required', false);
    }
  }

}

angular.module('core.transfer').controller('transferFormCtrl', transferFormCtrl);
