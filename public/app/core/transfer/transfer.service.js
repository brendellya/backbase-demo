/**
 * Created by Brendellya on 4/1/2017.
 */
function TransferService($http){
  var url = 'data/transactions.json';


  function getUniqueItems(list, keyword){
    var found = [];
    if(!list.length) return [];

    return list.reduce(function (memo, row) {
      if(found.indexOf(row.merchant) < 0){
        memo.push({
          merchant: row.merchant,
          merchantLogo: row.merchantLogo
        });

        found.push(row.merchant);
      }
        return memo;
    }, []);
  }

  function preProccessData(data){
    return data.map(function (row) {
        var hue = Math.ceil(Math.random() * 360);
        var hsl = 'hsl(' +hue+ ',50%,50%)';
        
        row.color = hsl;
        return row;
    });
  }

  return {
    // Local Cache
    cache: {
      transfers: []
    },

    // Fake Account
    account: {
      name: 'Free Checking(4692)',
      balance: 5824.76
    },

    updateBalance: function(payload){
      var newBalance  = this.account.balance - payload.amount;
      this.account.balance = newBalance.toFixed(2);
    },

    getData: function(){
      return $http.get(url).then((res) => {
        var data = res.data.data || [];

        // Preprocess Transfer Data
        this.cache.transfers = preProccessData(data);
      });
    },

    postTransfer: function(payload){
      this.cache.transfers.unshift(payload);
      this.updateBalance(payload);
    },

    getMerchants: function(){
      return getUniqueItems(this.cache.transfers, 'merchant');
    }
  };
}

angular.module('core.transfer').factory('TransferService', TransferService);
