/**
 * Created by Brendellya on 4/1/2017.
 */

var transfer = {
  bindings: {
    transfers: '<'
  },
  transclude: true,
  templateUrl: 'app/core/transfer/transfer.html',
  controller: 'transferCtrl'
};

angular.module('core.transfer').component('transfer', transfer);
