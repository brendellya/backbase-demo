/**
 * Created by Brendellya on 4/1/2017.
 */

var appHome = {
  bindings: {
    transfers: '<',
    balance: '<'
  },
  templateUrl: 'app/common/home/app-home.html'
};

angular.module('common').component('appHome', appHome)
  .config(function($stateProvider, $urlRouterProvider){
    $stateProvider
      .state('home', {
        url: '/',
        component: 'appHome',
        resolve: {
          transfers: function (TransferService) {
            return TransferService.getData().then(function(){
              return TransferService.cache.transfers;
            });
          },
          account: function(TransferService){
            return TransferService.account;
          }
        }
      });

      $urlRouterProvider.otherwise('/');
  });
