/**
 * Created by Brendellya on 4/1/2017.
 */

var appHeader = {
  templateUrl: 'app/common/header/app-header.html'
};

angular.module('common').component('appHeader', appHeader);
