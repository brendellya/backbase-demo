/**
 * Created by Brendellya on 4/1/2017.
 */

var appRoot = {
  template: '<div class="root container-fluid"><app-header></app-header><div ui-view></div></div>'
};


angular.module('app').component('appRoot', appRoot);
